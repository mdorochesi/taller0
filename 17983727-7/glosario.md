#Definición de Conceptos

1. **Control de Versiones (VC):** Se conoce como control de versiones a la gestión de diversos cambios que se realizan sobre un archivo, carpeta, proyecto, etc.

    **Características:**
    
        * Posee un mencanismo de almacenamiento de los elementos que se deben gestionar.
        * Permite realizar cambios sobre los archivos almacenados.
        * Permite llevar un registro historico de cambios realizados
        
    [Fuente](https://es.wikipedia.org/wiki/Control_de_versiones)

2. **Control de Versiones Distribuido (DVC):** Se conoce como control de versiones distribuido a un tipo de *control de versión* con la característica particular
que todo el código base así como el historial de cambios está disponible en los computadores de cada uno de los colaboradores de un proyecto.

    **Características:**
    
        * Permite trabajar a los usuarios de forma productiva sin la necesidad de estar conectados a una misma red.
        * Las operaciones básicas como (commit, push , pull) son más rapidas puesto que no hay necesidad de comunicarse a un servidor centralizado.
        * Permite el trabajo modularizado entre distintos desarrolladores.
        
    [Fuente](https://en.wikipedia.org/wiki/Distributed_version_control)

3. **Repositorio remoto y Repositorio local:** Un repositorio local es básicamente un repositorio que está en alguna máquina local, a la cual no se tiene un acceso externo a una cierta red. En general se ocupan en compañias que poseen
redes cerradas. En cambio, un repositorio remoto es un repositorio que se encuentra disponible en un servidor público por lo cual cualquier colaborador con acceso al repositorio podrá utilizar las operaciones básicas.

    [Fuente](https://www.git-tower.com/learn/git/ebook/en/command-line/remote-repositories/introduction)

4. **Copia de trabajo:**  Son los archivos en los cuales se está trabajando, ya sea un archivo en específico o un branch.

    [Fuente](https://www.google.com)

5. **Área de preparación:**  El área de preparación es un archivo, generalmente ubicado en el directorio de Git, que almacena información sobre lo que se incluirá en su próximo commit. Es conocido también como index.

    [Logo]:https://i.stack.imgur.com/zLTpo.png
    ![Imagen][logo]
    
    [Fuente](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

6. **Preparar cambios:** Es el proceso previo a confirmar los cambios en donde
se agregan todos los archivos que posteriormente seran cambiados. Se agregan estos archivos 
ocupando el comando ***add***.

7. **Confirmar cambios:** Se confirman los cambios que previamente se encontraban en preparar cam

    [Logo]:https://git-scm.com/figures/18333fig0201-tn.png
    ![Imagen][logo]
    
    [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

8. **Commit:** Es el comando que permite confirmar los cambios y además agregar un mensaje a los cambios.

    ```bash
    git commit -m "Se corrige error en input"
    ```

9. **Clone:** Clona un repositorio remoto creando un repositorio local.

     ```bash
     git clone url-git
    ```

10. **Pull:** Actualiza el repositorio local desde la información almacenada en el repositorio remoto. En estricto rigor
el comando pull hace un fetch y un merge.

     ```bash
     git pull
    ```

11. **Push:** Actualiza el repositorio remoto desde la información del repositorio local.

     ```bash
     git push
    ```

12. **Fetch:** Similar a Git pull con la diferencia de que solo descarga la información del repositorio remoto
pero no sobre-escribe ningún archivo local.

     ```bash
     git fetch
    ```

    [Fuente](https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull)

13. **Merge:** Sirve para fusionar diferentes ramas o versiones previas en una rama actual.

    [Fuente](https://git-scm.com/docs/git-merge)

14. **Status:** Muestra diferencias (añadido, editado o eliminado) entre el archivo index y el commit actual.

     ```bash
     git status
    ```

    [Fuente](https://git-scm.com/docs/git-status)

15. **Log:** Muestra los logs de los commmit.

16. **Checkout:** Permite cambiar la rama actual de trabajo.

     ```bash
     git checkout -b rama_nueva
    ```

17. **Branch:** Una rama es un nuevo camino de trabajo que se suele utilizar para administrar versiones de producción, desarrollo etc.
   El comando branch permite crear o borrar ramas.

18. **Etiqueta:** Sirve para etiquetar desde un commit hacia adelante. Por ejemplo si uno tenía una version incial 1.0 y luego de hacer varios cambios 
decide hacer un upgrade a la 2.0 se puede generar esa etiqueta para que en el registro
historico del proyecto se sepa en que momento se paso a la version 2.0